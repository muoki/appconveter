package com.example.kiptum.app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText edittext1, edittext2;
    private Button buttonConvert;
    private Button buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        //getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        edittext1 = findViewById(R.id.editText1);
        edittext2 = findViewById(R.id.editText2);
        buttonConvert = findViewById(R.id.button);
        buttonExit = findViewById(R.id.button2);

        buttonConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value=edittext1.getText().toString();
                int v=Integer.parseInt(value);
                double convert=v/25.4;
                edittext2.setText(String.valueOf(convert));
                edittext2.setEnabled(false);
                Toast.makeText(getApplicationContext(),String.valueOf(convert), Toast.LENGTH_LONG).show();
            }
        });
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}